// console.log ("Hello World");

/*function printInfo(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, "+ nickname);
}

printInfo(); //Invoke or call*/

				//parameter
function printName(firstName){
	console.log("My name is "+ firstName);
}

printName("Juana"); //argument
printName("John");
printName("Cena");



// Now we have a reusable function or reusable task but coud have different output based on what value to process with the help off..
// [SECTION] Parameters and Arguments

// Parameter
	// "firstName" is called a parameter
	// A "parameter" acts as a named variable/container that exists only inside a function
	// it is used to store information that is provided to a function when it is called/invoke


// Argument
	// "Juana", "John", and "Cena" the information/data provided directly into the function is called "argument".
	// Values passed when invoking a function are called arguments
	// These arguments are then stored as the parameter within the function.

let sampleVariable = "Inday";

printName(sampleVariable);	
// Variables can also be passed as an argument.
//-----------------------------
console.log("-----------------")

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of "+num+" divided by 8 is: "+ remainder);
	let isDivisibilityBy8 = remainder === 0;
	console.log("Is "+num+" divisible by 8?");
	console.log(isDivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

//[SECTION] Function as argument
	// Function parameters can also accept functions as arguments 
	// Some complex functions uses other function to perform more complicated results.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}
	//FOR FUTURE REFERENCE
	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

	// ---------------
	console.log("----------")

// [SECTION] Using Multiple Parameters

	function createFullName(firstName, middleName, lastName){
		console.log("My full name is "+ firstName + " " + middleName + " " + lastName);

	}	
		// Arguments follow the order of parameter.
	createFullName("James", "Christian", "Ocañada");

	// Using variables as an arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	function getDifferenceOf8Minus4(numA, numB){
		console.log("Diffence: "+ (numA - numB));
	}

	getDifferenceOf8Minus4(8,4);
	//getDifferenceOf8Minus4(4,8); //This will result to logical error

// [SECTION] Return statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called	
	function returnFullName(firstName, middleName, lastName){
		// return firstName + " " + middleName + " "+ lastName;
		
		// We could also create a variable inside the function to contain the result and return the variable inside
		let fullName = firstName + " " + middleName + " "+ lastName;
		return fullName;

		// This line of code will not be printed cuz after of return keyword
		console.log("this is printed inside a function");
	}

	let completeName = returnFullName("Paul", "Smith", "Jordan");
	console.log(completeName);

	console.log("I am "+ completeName);




	function printPlayerInfo(userName, level, job){
		console.log("Username: "+ userName);
		console.log("Level: "+ level);
		console.log("Job: "+ job);
	}

	printPlayerInfo("boxzMapagmahal", "Senior", "Programmer");
	console.log(user1); // returns undefined
